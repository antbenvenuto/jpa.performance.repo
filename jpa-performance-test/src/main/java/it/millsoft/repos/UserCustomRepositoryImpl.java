package it.millsoft.repos;

import it.millsoft.entities.*;
import it.millsoft.projections.UserMinimalDataProjectionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class UserCustomRepositoryImpl implements IUserCustomRepository
{

    @Autowired
    private EntityManager em;

    @Override
    public List<UserEntity> getUserByFilter()
    {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<UserEntity> cq = cb.createQuery(UserEntity.class);
        Root<UserEntity> user = cq.from(UserEntity.class);

        // Fetch User --> Address --> AddressType
        Fetch<UserEntity, AddressEntity> addressFetch = user.fetch(UserEntity_.address, JoinType.LEFT);
//        Fetch<AddressEntity, AddressTypeEntity> addressTypeFetch = addressFetch.fetch(AddressEntity_.addressType, JoinType.LEFT);
        // Fetch User --> Badge --> BadgeType
        Fetch<UserEntity, BadgeEntity> badgeFetch = user.fetch(UserEntity_.badge, JoinType.LEFT);
//        Fetch<BadgeEntity, BadgeTypeEntity> badgeTypeFetch = badgeFetch.fetch(BadgeEntity_.badgeType, JoinType.LEFT);

        List<Predicate> predicates = new ArrayList<Predicate>();
//        Path<Long> idContributoEdiserPath = contributo.get(ContributoEntity_.idContributoEdiser);
//        Predicate idContributoEdiserPredicate = cb.equal(idContributoEdiserPath, new Long(8));
//        predicates.add(idContributoEdiserPredicate);

        cq.where(cb.and(predicates.toArray(new Predicate[predicates.size()]))).distinct(true);
        TypedQuery<UserEntity> query = em.createQuery(cq);
        List<UserEntity> result = query.getResultList();

        return result;
    }

    @Override
    public List<UserEntity> getUserByFilterWithGraph()
    {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<UserEntity> cq = cb.createQuery(UserEntity.class);
        Root<UserEntity> user = cq.from(UserEntity.class);

        List<Predicate> predicates = new ArrayList<Predicate>();
        cq.where(cb.and(predicates.toArray(new Predicate[predicates.size()]))).distinct(true);

        TypedQuery<UserEntity> query = em.createQuery(cq);
        query.setHint("javax.persistence.fetchgraph", em.getEntityGraph("graph-search-user"));  //em.getEntityGraph("graph-search-user") // Tutte le relazioni indicate nel GRAFO sono trattate come EADGER
        List<UserEntity> result = query.getResultList();
        return result;
    }

    @Override
    public List<UserMinimalDataProjectionDTO> findUser_ByUserId___CRITERIA___WITH_DTO_PROJECTION(Integer userId)
    {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<UserMinimalDataProjectionDTO> cq = cb.createQuery(UserMinimalDataProjectionDTO.class);
        Root<UserEntity> root = cq.from(UserEntity.class);

        List<Predicate> predicates = new ArrayList<Predicate>();

        Path<Integer> userIdPath = root.get(UserEntity_.userId);
        Predicate userIdPredicate = cb.equal(userIdPath,userId);
        predicates.add(userIdPredicate);

        cq.where(cb.and(predicates.toArray(new Predicate[predicates.size()]))).distinct(true);
        cq.select(
                cb.construct(
                        UserMinimalDataProjectionDTO.class,
                        root.get(UserEntity_.userId),
                        root.get(UserEntity_.name),
                        root.get(UserEntity_.surname),
                        root.get(UserEntity_.email),
                        root.get(UserEntity_.address).get(AddressEntity_.addressId),
                        root.get(UserEntity_.address).get(AddressEntity_.description),
                        root.get(UserEntity_.address).get(AddressEntity_.city),
                        root.get(UserEntity_.address).get(AddressEntity_.state),
                        root.get(UserEntity_.address).get(AddressEntity_.addressType).get(AddressTypeEntity_.addressTypeId)
                )
        );

        TypedQuery<UserMinimalDataProjectionDTO> query = em.createQuery(cq);
        List<UserMinimalDataProjectionDTO> result = query.getResultList();

        return result;
    }

    @Override
    public List<UserMinimalDataProjectionDTO> findUser_All___CRITERIA___WITH_DTO_PROJECTION()
    {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<UserMinimalDataProjectionDTO> cq = cb.createQuery(UserMinimalDataProjectionDTO.class);
        Root<UserEntity> root = cq.from(UserEntity.class);

        Join<UserEntity, AddressEntity> addressJoin = root.join(UserEntity_.address, JoinType.LEFT);

        cq.where().distinct(true);
        cq.select(
                cb.construct(
                        UserMinimalDataProjectionDTO.class,
                        root.get(UserEntity_.userId),
                        root.get(UserEntity_.name),
                        root.get(UserEntity_.surname),
                        root.get(UserEntity_.email),
                        addressJoin.get(AddressEntity_.addressId),
                        addressJoin.get(AddressEntity_.description),
                        addressJoin.get(AddressEntity_.city),
                        addressJoin.get(AddressEntity_.state),
                        addressJoin.get(AddressEntity_.addressType).get(AddressTypeEntity_.addressTypeId)
                )
        );

        TypedQuery<UserMinimalDataProjectionDTO> query = em.createQuery(cq);
        List<UserMinimalDataProjectionDTO> result = query.getResultList();

        return result;
    }

    @Override
    public List<Tuple> findUser_ByUserId___JPQL___WITH_TUPLE(Integer userId)
    {
        TypedQuery<Tuple> query = em.createQuery("SELECT u.name,u.surname,u.email FROM UserEntity u", Tuple.class);
        List<Tuple> result = query.getResultList();
        return result;
    }
}
