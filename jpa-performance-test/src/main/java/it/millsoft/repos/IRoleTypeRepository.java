package it.millsoft.repos;

import it.millsoft.entities.BadgeTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IRoleTypeRepository extends JpaRepository<BadgeTypeEntity,Integer>
{
}
