package it.millsoft.repos;

import it.millsoft.entities.UserEntity;
import it.millsoft.projections.UserMinimalDataProjection;
import it.millsoft.projections.UserMinimalDataProjectionDTO;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

import static org.springframework.data.jpa.repository.EntityGraph.EntityGraphType.FETCH;

public interface IUserRepository extends JpaRepository<UserEntity,Integer>
{

    /**** Accesso Diretto ****/

    @Query("SELECT u FROM UserEntity u " +
           "WHERE u.userId = :userId")
    UserEntity findBy_UserId___JPQL___WITHOUT_EXPLICIT_FECTH(@Param("userId") Integer userId);

    @Query("SELECT u FROM UserEntity u " +
           "LEFT JOIN FETCH u.address a " +
           "LEFT JOIN FETCH u.badge   b " +
           "WHERE u.userId = :userId ")
    UserEntity findBy_UserId___JPQL___WITH_EXPLICIT_FECTH(@Param("userId") Integer userId);

    /***** Via Query *****/

    UserMinimalDataProjection findByUserId(Integer userId);

    @Query("SELECT new it.millsoft.projections.UserMinimalDataProjectionDTO(u.userId,u.name,u.surname,u.email,u.address.addressId,u.address.description,u.address.city,u.address.state,u.address.addressType.addressTypeId)" +
           "FROM UserEntity u " +
           "WHERE u.userId = :userId")
    UserMinimalDataProjectionDTO findBy_UserId___JPQL___WITH_DTO_PRJOECTION(Integer userId);

    @Query("SELECT new it.millsoft.projections.UserMinimalDataProjectionDTO(u.userId," +
                                                                           "u.name," +
                                                                           "u.surname," +
                                                                           "u.email," +
                                                                           "add.addressId," +
                                                                           "add.description," +
                                                                           "add.city," +
                                                                           "add.state," +
                                                                           "add.addressType.addressTypeId)" +
                                                                           "FROM UserEntity u " +
                                                                           "LEFT JOIN u.address add ")
    Set<UserMinimalDataProjectionDTO> findBy_All___JPQL___WITH_DTO_PRJOECTION();

}
