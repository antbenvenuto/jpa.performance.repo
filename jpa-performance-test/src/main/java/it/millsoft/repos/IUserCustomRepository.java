package it.millsoft.repos;

import it.millsoft.entities.UserEntity;
import it.millsoft.projections.UserMinimalDataProjectionDTO;
import org.springframework.data.jpa.repository.EntityGraph;

import javax.persistence.Tuple;
import java.util.List;

import static org.springframework.data.jpa.repository.EntityGraph.EntityGraphType.FETCH;

public interface IUserCustomRepository
{
    List<UserEntity> getUserByFilter();

    @EntityGraph(value="graph-search-user", type=FETCH)
    List<UserEntity> getUserByFilterWithGraph();

    List<UserMinimalDataProjectionDTO> findUser_ByUserId___CRITERIA___WITH_DTO_PROJECTION(Integer userId);

    List<UserMinimalDataProjectionDTO> findUser_All___CRITERIA___WITH_DTO_PROJECTION();

    List<Tuple> findUser_ByUserId___JPQL___WITH_TUPLE(Integer userId);


}
