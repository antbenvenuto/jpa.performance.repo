package it.millsoft.projections;

public class UserMinimalDataProjectionDTO
{
    private Integer userId;
    private String name;
    private  String surname;
    private String email;
    private Integer addressId;
    private String addressDescription;
    private String city;
    private String state;
    private Integer addressTypeId;

    public UserMinimalDataProjectionDTO(Integer userId, String name, String surname, String email, Integer addressId, String addressDescription, String city, String state,Integer addressTypeId) {
        this.userId = userId;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.addressId = addressId;
        this.addressDescription = addressDescription;
        this.city = city;
        this.state = state;
        this.addressTypeId = addressTypeId;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public Integer getAddressId() {
        return addressId;
    }

    public String getAddressDescription() {
        return addressDescription;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }
}
