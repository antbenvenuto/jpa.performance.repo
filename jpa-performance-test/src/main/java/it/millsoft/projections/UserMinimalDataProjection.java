package it.millsoft.projections;

public interface UserMinimalDataProjection
{
    Integer getUserId();
    String getName();
    String getSurname();
    String getEmail();
    AddressMinimalDataProjection getAddress();

    interface AddressMinimalDataProjection
    {
        Integer getAddressId();
        String getDescription();
        String getCity();
        String getState();
    }

}
