package it.millsoft.entities;

import javax.persistence.*;

@Entity
@Table(name = "address_types", schema = "test_db")
public class AddressTypeEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "ADDRESS_TYPE_ID")
    private Integer addressTypeId;

    @Column(name = "CODE")
    private String code;


    public Integer getAddressTypeId() {
        return addressTypeId;
    }

    public void setAddressTypeId(Integer addressTypeId) {
        this.addressTypeId = addressTypeId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "AddressTypeEntity{" +
                "addressTypeId=" + addressTypeId +
                ", code='" + code + '\'' +
                '}';
    }
}
