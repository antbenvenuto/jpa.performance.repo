package it.millsoft.entities;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "docs", schema = "test_db")
public class DocEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "DOC_ID")
    private Integer docId;

    @Basic
    @Column(name = "NAME")
    private String name;

    @Basic
    @Column(name = "VALID_FROM")
    private LocalDateTime validFrom;

    @Basic
    @Column(name = "VALID_TO")
    private LocalDateTime validTo;

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private UserEntity user;

    public Integer getDocId() {
        return docId;
    }

    public void setDocId(Integer docId) {
        this.docId = docId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(LocalDateTime validFrom) {
        this.validFrom = validFrom;
    }

    public LocalDateTime getValidTo() {
        return validTo;
    }

    public void setValidTo(LocalDateTime validTo) {
        this.validTo = validTo;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "DocEntity{" +
                "docId=" + docId +
                ", name='" + name + '\'' +
                ", validFrom=" + validFrom +
                ", validTo=" + validTo +
                ", user=" + user +
                '}';
    }
}
