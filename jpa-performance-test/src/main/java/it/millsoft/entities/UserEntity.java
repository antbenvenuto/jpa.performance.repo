package it.millsoft.entities;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "users", schema = "test_db")
@NamedEntityGraph(name = "graph-search-user",
        attributeNodes =
                {
                        @NamedAttributeNode(value = "badge",subgraph = "badge-subgraph"),
                        @NamedAttributeNode(value = "address",subgraph = "address-subgraph")
                },
        subgraphs =
                {
                        @NamedSubgraph(name = "badge-subgraph",
                                attributeNodes = {
                                        @NamedAttributeNode(value = "badgeType")
                                }
                        ),
                        @NamedSubgraph(name = "address-subgraph",
                                attributeNodes = {
                                        @NamedAttributeNode(value = "addressType")
                                }
                        ),
                }
)
public class UserEntity
{

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "USER_ID")
    private Integer userId;

    @Column(name = "NAME")
    private String name;

    @Column(name = "SURNAME")
    private String surname;

    @Column(name = "EMAIL")
    private String email;

    /**
     *
     * RELAZIONE 'OBBLIGATORIA'
     * Scelta 1: Recuperata OnDemand       [Lazy]   --> Deve essere impostata l'annotation @LazyToOne, senza @Fetch(Join), che aggiungerebbe la relazione il Left Join
     * Scelta 2: Recuperata immediatamente [Eadger] --> Deve essere impostata l'annotation @Fetch(Join),
     *
     */
//    @Fetch(FetchMode.JOIN)
    @OneToOne(fetch = FetchType.LAZY)
    @LazyToOne(LazyToOneOption.NO_PROXY)
    @JoinColumn(name = "BADGE_ID")
    private BadgeEntity badge;

    /**
     *
     * RELAZIONE 'NON OBBLIGATORIA'
     * Scelta 1: Recuperata OnDemand       [Lazy]   --> Deve essere impostata l'annotation @LazyToOne, senza @Fetch(Join), che aggiungerebbe la relazione il Left Join
     * Scelta 2: Recuperata immediatamente [Eadger] --> Deve essere impostata l'annotation @Fetch(Join),
     *
     */
//    @Fetch(FetchMode.JOIN)
    @OneToOne(fetch = FetchType.LAZY)
//    @LazyToOne(LazyToOneOption.NO_PROXY)
    @JoinColumn(name = "ADDRESS_ID")
    private AddressEntity address;

    @OneToMany(mappedBy = "user",fetch = FetchType.LAZY)
    private List<DocEntity> docs;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public BadgeEntity getBadge() {
        return badge;
    }

    public void setBadge(BadgeEntity badge) {
        this.badge = badge;
    }

    public AddressEntity getAddress() {
        return address;
    }

    public void setAddress(AddressEntity address) {
        this.address = address;
    }

    public List<DocEntity> getDocs() {
        return docs;
    }

    public void setDocs(List<DocEntity> docs) {
        this.docs = docs;
    }

    @Override
    public String toString() {
        return "UserEntity{" +
                "userId=" + userId +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", badge=" + badge +
                ", address=" + address +
//                ", docs=" + docs +
                '}';
    }
}
