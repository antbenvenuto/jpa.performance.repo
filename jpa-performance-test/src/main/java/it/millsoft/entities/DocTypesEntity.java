package it.millsoft.entities;

import javax.persistence.*;

@Entity
@Table(name = "doc_types", schema = "test_db")
public class DocTypesEntity
{
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "DOC_TYPE_ID")
    private Integer docTypeId;

    @Basic
    @Column(name = "CODE")
    private String code;


    public Integer getDocTypeId() {
        return docTypeId;
    }

    public void setDocTypeId(Integer docTypeId) {
        this.docTypeId = docTypeId;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "DocTypesEntity{" +
                "docTypeId=" + docTypeId +
                ", code='" + code + '\'' +
                '}';
    }
}
