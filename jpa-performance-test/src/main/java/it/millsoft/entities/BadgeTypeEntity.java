package it.millsoft.entities;

import javax.persistence.*;

@Entity
@Table(name = "badge_types", schema = "test_db")
public class BadgeTypeEntity
{

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "BADGE_TYPE_ID")
    private Integer badgeTypeId;

    @Column(name = "CODE")
    private String code;

    public Integer getBadgeTypeId() {
        return badgeTypeId;
    }

    public void setBadgeTypeId(Integer badgeTypeId) {
        this.badgeTypeId = badgeTypeId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "BadgeTypeEntity{" +
                "badgeTypeId=" + badgeTypeId +
                ", code='" + code + '\'' +
                '}';
    }
}
