package it.millsoft.entities;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

@Entity
@Table(name = "badges", schema = "test_db")
public class BadgeEntity
{

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "BADGE_ID", nullable = false)
    private Integer badgeId;

    @Column(name = "CODE", nullable = false, length = 10)
    private String code;

    @Column(name = "DESCRIPTION", nullable = false, length = 255)
    private String description;

    @Fetch(FetchMode.JOIN)
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BADGE_TYPE_ID")
    private BadgeTypeEntity badgeType;

    public Integer getBadgeId() {
        return badgeId;
    }

    public void setBadgeId(Integer badgeId) {
        this.badgeId = badgeId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BadgeTypeEntity getBadgeType() {
        return badgeType;
    }

    public void setBadgeType(BadgeTypeEntity badgeType) {
        this.badgeType = badgeType;
    }

    @Override
    public String toString() {
        return "BadgeEntity{" +
                "badgeId=" + badgeId +
                ", code='" + code + '\'' +
                ", description='" + description + '\'' +
                ", badgeType=" + badgeType +
                '}';
    }
}
