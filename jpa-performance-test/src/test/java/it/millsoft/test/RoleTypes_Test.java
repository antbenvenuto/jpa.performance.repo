package it.millsoft.test;

import it.millsoft.entities.BadgeTypeEntity;
import it.millsoft.repos.IRoleTypeRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {RepositoryTestContext.class})
@SpringBootTest
public class RoleTypes_Test
{

    @Autowired
    private IRoleTypeRepository roleTypeRepository;

    @Test
    public void insertUser_Test()
    {
        try
        {
            BadgeTypeEntity eRoleType = new BadgeTypeEntity();
            eRoleType.setCode("AMM");
            eRoleType = roleTypeRepository.save(eRoleType);
            Assert.assertNotNull(eRoleType.getBadgeTypeId());
            System.out.println("Role Type ID: " + eRoleType.getBadgeTypeId());
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            Assert.fail(ex.getMessage());
        }
    }

}
