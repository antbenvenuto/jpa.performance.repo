package it.millsoft.test;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableAutoConfiguration
@ComponentScan(value = "it.millsoft")
@EntityScan({"it.millsoft.entities"})
@EnableJpaRepositories("it.millsoft.repos")
public class RepositoryTestContext
{


}
