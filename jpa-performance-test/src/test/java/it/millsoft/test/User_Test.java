package it.millsoft.test;

import it.millsoft.entities.BadgeEntity;
import it.millsoft.entities.UserEntity;
import it.millsoft.projections.UserMinimalDataProjection;
import it.millsoft.projections.UserMinimalDataProjectionDTO;
import it.millsoft.repos.IUserCustomRepository;
import it.millsoft.repos.IUserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Tuple;
import java.util.List;
import java.util.Set;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {RepositoryTestContext.class})
@SpringBootTest
public class User_Test
{

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private IUserCustomRepository userCustomRepository;

    @Autowired
    private EntityManager em;

    @Test
    public void getUser_DIRECT_FETCH_Test()
    {
        try
        {
            UserEntity eUser = em.find(UserEntity.class,1);
            Assert.assertNotNull(eUser);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void getUser_DIRECT_FETCH_2_Test()
    {
        try
        {
            UserEntity eUser = userRepository.findById(2).get();
            Assert.assertNotNull(eUser);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void findBy_UserId_findBy_UserId_JPQL_WITHOUT_EXPLICIT_FECTH_Test()
    {
        try
        {
            UserEntity eUser = userRepository.findBy_UserId___JPQL___WITHOUT_EXPLICIT_FECTH(1);
            Assert.assertNotNull(eUser);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void findBy_UserId_findBy_UserId_JPQL_WITH_EXPLICIT_FECTH_Test()
    {
        try
        {
            UserEntity eUser = userRepository.findBy_UserId___JPQL___WITH_EXPLICIT_FECTH(1);
            Assert.assertNotNull(eUser);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void findBy_UserId_findBy_UserId_WITH_CUSTOM_PROJECTION_Test()
    {
        try
        {
            UserMinimalDataProjection pUser = userRepository.findByUserId(1);
            Assert.assertNotNull(pUser);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void findBy_UserId_findBy_UserId_WITH_DTO_PROJECTION_Test()
    {
        try
        {
            UserMinimalDataProjectionDTO pUser = userRepository.findBy_UserId___JPQL___WITH_DTO_PRJOECTION(1);
            Assert.assertNotNull(pUser);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void findBy_All_WITH_DTO_PRJOECTION_Test()
    {
        try
        {
            Set<UserMinimalDataProjectionDTO> pUsers = userRepository.findBy_All___JPQL___WITH_DTO_PRJOECTION();
            Assert.assertNotNull(pUsers);
            Assert.assertEquals(2,pUsers.size());
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void findUser_ByUserId_WITH_CRITERIA_AND_PROJECTION_Test()
    {
        try
        {
            List<UserMinimalDataProjectionDTO> pUsers = userCustomRepository.findUser_ByUserId___CRITERIA___WITH_DTO_PROJECTION(1);
            Assert.assertNotNull(pUsers);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void findUser_All___CRITERIA___WITH_DTO_PROJECTION_Test()
    {
        try
        {
            List<UserMinimalDataProjectionDTO> pUsers = userCustomRepository.findUser_All___CRITERIA___WITH_DTO_PROJECTION();
            Assert.assertNotNull(pUsers);
            Assert.assertEquals(2,pUsers.size());
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void findUser_ByUserId___JPQL___WITH_TUPLE_Test()
    {
        List<Tuple> data = userCustomRepository.findUser_ByUserId___JPQL___WITH_TUPLE(1);
        Assert.assertNotNull(data);
    }


    @Test
    @Transactional
    public void getUserByFilter_Test()
    {
        try
        {
//            List<UserEntity> eUsers = userCustomRepository.getUserByFilter();
            UserEntity eUser = userCustomRepository.getUserByFilter().get(0);
            System.out.println("\n********************************");
            System.out.println("Name    : " + eUser.getName());
            System.out.println("Surname : " + eUser.getSurname());
            System.out.println("Email   : " + eUser.getEmail());
            System.out.println("********************************\n");
            BadgeEntity eBadge = eUser.getBadge();
            System.out.println("Badge Code : " + eBadge.getCode());
//            for(UserEntity eUser : eUsers)
//            {
//                eUser.toString();
//            }
            Assert.assertNotNull(eUser);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    @Transactional
    public void getUserByFilterWithGraph_Test()
    {
        try
        {
            List<UserEntity> eUsers = userCustomRepository.getUserByFilterWithGraph();
            for(UserEntity eUser : eUsers)
            {
                System.out.println(eUser.toString());
            }
            Assert.assertNotNull(eUsers);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            Assert.fail(ex.getMessage());
        }
    }

}
