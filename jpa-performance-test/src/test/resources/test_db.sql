/*
 Navicat Premium Data Transfer

 Source Server         : ___TEST
 Source Server Type    : MySQL
 Source Server Version : 80013
 Source Host           : localhost:3306
 Source Schema         : test_db

 Target Server Type    : MySQL
 Target Server Version : 80013
 File Encoding         : 65001

 Date: 20/02/2019 15:20:40
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for address_types
-- ----------------------------
DROP TABLE IF EXISTS `address_types`;
CREATE TABLE `address_types`  (
  `ADDRESS_TYPE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`ADDRESS_TYPE_ID`) USING BTREE,
  INDEX `ADDRESS_TYPE_ID`(`ADDRESS_TYPE_ID`) USING BTREE,
  INDEX `ADDRESS_TYPE_ID_2`(`ADDRESS_TYPE_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of address_types
-- ----------------------------
INSERT INTO `address_types` VALUES (1, 'CS');
INSERT INTO `address_types` VALUES (2, 'UFF');

-- ----------------------------
-- Table structure for addresses
-- ----------------------------
DROP TABLE IF EXISTS `addresses`;
CREATE TABLE `addresses`  (
  `ADDRESS_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `CITY` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `STATE` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `ADDRESS_TYPE_ID` int(11) NOT NULL,
  PRIMARY KEY (`ADDRESS_ID`) USING BTREE,
  INDEX `ADDRESS_TYPE_ID`(`ADDRESS_TYPE_ID`) USING BTREE,
  INDEX `ADDRESS_ID`(`ADDRESS_ID`) USING BTREE,
  CONSTRAINT `addresses_ibfk_1` FOREIGN KEY (`ADDRESS_TYPE_ID`) REFERENCES `address_types` (`address_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of addresses
-- ----------------------------
INSERT INTO `addresses` VALUES (1, 'Via Italia', 'Rome', 'IT', 1);

-- ----------------------------
-- Table structure for badge_types
-- ----------------------------
DROP TABLE IF EXISTS `badge_types`;
CREATE TABLE `badge_types`  (
  `BADGE_TYPE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`BADGE_TYPE_ID`) USING BTREE,
  INDEX `ROLE_TYPE_ID`(`BADGE_TYPE_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of badge_types
-- ----------------------------
INSERT INTO `badge_types` VALUES (1, 'AMM');
INSERT INTO `badge_types` VALUES (2, 'DEL');
INSERT INTO `badge_types` VALUES (3, 'USR');

-- ----------------------------
-- Table structure for badges
-- ----------------------------
DROP TABLE IF EXISTS `badges`;
CREATE TABLE `badges`  (
  `BADGE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `BADGE_TYPE_ID` int(11) NOT NULL,
  PRIMARY KEY (`BADGE_ID`) USING BTREE,
  INDEX `ROLE_TYPE_ID`(`BADGE_TYPE_ID`) USING BTREE,
  INDEX `ROLE_ID`(`BADGE_ID`) USING BTREE,
  CONSTRAINT `badges_ibfk_1` FOREIGN KEY (`BADGE_TYPE_ID`) REFERENCES `badge_types` (`BADGE_TYPE_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of badges
-- ----------------------------
INSERT INTO `badges` VALUES (1, 'AA11223344', 'A simple badge', 1);
INSERT INTO `badges` VALUES (2, 'BB44332211', 'Another simple badge', 2);
INSERT INTO `badges` VALUES (3, 'CC55667788', 'A complex badge', 3);

-- ----------------------------
-- Table structure for doc_types
-- ----------------------------
DROP TABLE IF EXISTS `doc_types`;
CREATE TABLE `doc_types`  (
  `DOC_TYPE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`DOC_TYPE_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of doc_types
-- ----------------------------
INSERT INTO `doc_types` VALUES (1, 'PAT');
INSERT INTO `doc_types` VALUES (2, 'CF');
INSERT INTO `doc_types` VALUES (3, 'CI');
INSERT INTO `doc_types` VALUES (4, 'TM');

-- ----------------------------
-- Table structure for docs
-- ----------------------------
DROP TABLE IF EXISTS `docs`;
CREATE TABLE `docs`  (
  `DOC_ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `VALID_FROM` datetime(0) NOT NULL,
  `VALID_TO` datetime(0) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `DOC_TYPE_ID` int(11) NOT NULL,
  PRIMARY KEY (`DOC_ID`) USING BTREE,
  INDEX `USER_ID`(`USER_ID`) USING BTREE,
  INDEX `DOC_TYPE_ID`(`DOC_TYPE_ID`) USING BTREE,
  CONSTRAINT `docs_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `docs_ibfk_2` FOREIGN KEY (`DOC_TYPE_ID`) REFERENCES `doc_types` (`doc_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `USER_ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `SURNAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `EMAIL` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `BADGE_ID` int(11) NOT NULL,
  `ADDRESS_ID` int(11) NOT NULL,
  PRIMARY KEY (`USER_ID`) USING BTREE,
  INDEX `ROLE_ID`(`BADGE_ID`) USING BTREE,
  INDEX `ADDRESS_ID`(`ADDRESS_ID`) USING BTREE,
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`BADGE_ID`) REFERENCES `badges` (`BADGE_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `users_ibfk_2` FOREIGN KEY (`ADDRESS_ID`) REFERENCES `addresses` (`address_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Jhon', 'Doe', 'jhon.doe@gmail.com', 1, 1);

SET FOREIGN_KEY_CHECKS = 1;
